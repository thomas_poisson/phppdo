-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : jeu. 12 août 2021 à 18:57
-- Version du serveur :  10.4.19-MariaDB
-- Version de PHP : 8.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `bookmarks`
--

-- --------------------------------------------------------

--
-- Structure de la table `bookmarks`
--

CREATE TABLE `bookmarks` (
  `id` int(11) NOT NULL,
  `liens` varchar(255) NOT NULL,
  `noms` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `bookmarks`
--

INSERT INTO `bookmarks` (`id`, `liens`, `noms`) VALUES
(1, 'https://twitter.com/?lang=fr', 'twitter'),
(5, 'https://www.netflix.com/fr/login?nextpage=https%3A%2F%2Fwww.netflix.com%2Fbrowse', 'netflix'),
(6, 'https://www.crunchyroll.com/fr', 'crunchyroll'),
(7, 'https://www.wakanim.tv/fr/v2', 'wakanim'),
(8, 'https://www.instant-gaming.com/fr/?gclid=CjwKCAiAmNbwBRBOEiwAqcwwpUj4zZz1_X_6GBiNOlBxJwvrJK5WS5pUoc9jXiNfFcu0wmQFfvK_bxoCBSsQAvD_BwE', 'instant-gaming'),
(9, 'https://www.callofduty.com/fr/blackopscoldwar', 'cod_bocw');

-- --------------------------------------------------------

--
-- Structure de la table `bookmarks_categories`
--

CREATE TABLE `bookmarks_categories` (
  `id` int(20) NOT NULL,
  `bookmarks` int(11) NOT NULL,
  `categories` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `bookmarks_categories`
--

INSERT INTO `bookmarks_categories` (`id`, `bookmarks`, `categories`) VALUES
(2, 1, 1),
(6, 5, 2),
(7, 6, 2),
(8, 7, 2),
(9, 8, 3),
(10, 9, 3);

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) NOT NULL,
  `nom` varchar(50) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `categories`
--

INSERT INTO `categories` (`id`, `nom`, `description`) VALUES
(1, 'Réseaux sociaux', 'reseaux'),
(2, 'Streaming', 'streaming'),
(3, 'Gaming', 'gaming');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `bookmarks`
--
ALTER TABLE `bookmarks`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `bookmarks_categories`
--
ALTER TABLE `bookmarks_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bookmarks` (`bookmarks`),
  ADD KEY `categories` (`categories`);

--
-- Index pour la table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `bookmarks`
--
ALTER TABLE `bookmarks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT pour la table `bookmarks_categories`
--
ALTER TABLE `bookmarks_categories`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT pour la table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `bookmarks_categories`
--
ALTER TABLE `bookmarks_categories`
  ADD CONSTRAINT `bookmarks_categories_ibfk_1` FOREIGN KEY (`bookmarks`) REFERENCES `bookmarks` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `bookmarks_categories_ibfk_2` FOREIGN KEY (`categories`) REFERENCES `categories` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
