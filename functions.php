<!DOCTYPE html>
<html lang="fr_FR">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  <link href="functions.css" rel="stylesheet" type="text/css">
</head>
<?php


        include 'connect.php';

        


$query = $bdd->prepare("SELECT b.noms as 'name bookmark', 
                        b.liens as 'lien bookmark', 
                        b.id as 'id bookmark', 
                        c.nom as 'category name', 
                        c.description as 'category description' 
                        FROM categories as c 
                        inner join bookmarks_categories as bc ON c.id = bc.categories

                        inner join bookmarks as b ON b.id = bc.bookmarks;");
$query->execute();
$resultat = $query->fetchAll();

?>
<header>
    <h1 class="h1">Bookmarks</h1>
    <div class="button">
        <a href="modifyform.php"><button id="modif">Modification Bookmarks</button></a>
        <a href="deleteform.php"><button id="supp">Suppression Bookmarks</button></a>
        <a href="addform.php"><button id="ajt">Ajout Bookmarks</button></a>
    </div>
</header>
    <div class="table"> 
        <table>
            <thead>
                <tr>
                    <td class="noms">Nom</td>
                    <td class="liens">Liens</td>
                    <td class="catnom">Categories nom</td>
                    <td class="catdesc">Categories description</td>
                </tr>
        
    </thead>
    <tbody>
</div> 
       
<?php

foreach ($resultat as $key => $bookmark)
{

    echo "<tr>";
    echo "<td><a href='modifyform.php?id=".$bookmark['id bookmark']."'>".$bookmark['name bookmark']."</a></td>";
    echo "<td>".$bookmark['lien bookmark']."</td>";
    echo "<td>".$bookmark['category name']."</td>";
    echo "<td>".$bookmark['category description']."</td>";
    echo "<tr>";
}
?>
<?php



?>
</html>