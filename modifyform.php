<!DOCTYPE html>
<html lang="fr_FR">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  <link href="modifyform.css" rel="stylesheet" type="text/css">
</head>



<body>
  <header>
      <h1 class="h1">Modification bookmarks</h1>
      <div class="button">
        <a href="functions.php"><button id="book">Bookmarks</button></a>
        <a href="deleteform.php"><button id="supp">Suppression Bookmarks</button></a>
        <a href="addform.php"><button id="ajt">Ajout Bookmarks</button></a>
      </div>
  </header>
  <?php


  include 'connect.php';



  if (isset($_POST['id_cat'])) {


    $query = $bdd->prepare("UPDATE bookmarks SET noms = :noms, liens = :liens WHERE id = :id");
    $query2 = $bdd->prepare("UPDATE bookmarks_categories SET bookmarks = :bookmarks, categories= :categories WHERE bookmarks= :bookmarks");

    $query->bindParam(':noms', $_POST['noms']);
    $query->bindParam(':id', $_POST['id']);
    $query->bindParam(':liens', $_POST['liens']);
    $query2->bindParam(':bookmarks', $_POST['id']);
    $query2->bindParam(':categories', $_POST['id_cat']);

    $res = $query->execute();

  $resultat = $query2->execute();


  }

  $catset = $bdd->prepare("SELECT c.nom as 'name category', c.id as 'id category' FROM categories as c");
  $catset->execute();
  $catsetresult = $catset->fetchAll();

  $rep = $bdd->prepare("SELECT b.noms as 'noms', 
                  b.liens as 'liens', 
                  b.id as 'id',  c.nom as 'nom categories', bc.categories as 'id categories'
                  from bookmarks as b
                  inner join bookmarks_categories as bc ON b.id = bc.bookmarks
                  inner join categories as c ON c.id = bc.categories ");

  $rep->execute();
  $donnees = $rep->fetchAll();



  foreach ($donnees as $bookmark) {



  ?> 

    <form action="modifyform.php" method="post">
      <div class="form">
        <div class="form2">
          <label for="liens" id="liens">Lien </label> <input type="text"  name="liens" id="liens" value="<?php echo $bookmark["liens"] ?>" />
        </div>
        <div class="form3">   
          <label for="noms">Nom </label> <input type="text" id="noms"name="noms" value="<?php echo $bookmark["noms"] ?>" />
        </div>
        <div class="form4"> 
          <label for="id">Id </label> <input type="number" id="id "name="id" value="<?php echo $bookmark["id"] ?>" />
        </div> 
        <div class="select"> 
          <select size="1" name="id_cat" id="idcat">
            <option value="">Catégorie</option>
            <?php 
            $toselect='' ;
            $all=array('');
            foreach ($catsetresult as $cat) {
              if ($bookmark['id categories']==$cat['id category']){
                    echo '<option value="'.$cat['id category'].'" selected>'.$cat['name category'].'</option>';
              } else {
                echo '<option value="'.$cat['id category'].'">'.$cat['name category'].'</option>';
              } 
            } 
              ?>
              
          </select>
          <input type="submit" id="modif" value="Modifier" />
        </div>
      </div>  
    </form>
</body>

</html>
<?php
  }
?>